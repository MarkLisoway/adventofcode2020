import utilities.ResourceManager
import java.lang.StringBuilder
import java.lang.UnsupportedOperationException

fun main() {
    val rawInput = ResourceManager().getResourceAsText("Day4Input.txt")

    val input = rawInput
        .split("\r\n")
        .chunkPassportData()
        .deserializePassportData()

    val validPassports = input.count { passport -> passport.hasAllRequiredFields() }

    println(validPassports)

    val validStrictPassports = input.count { passport -> passport.isStrictValid() }

    println(validStrictPassports)
}

fun List<String>.chunkPassportData(): List<String> {
    val passportData = mutableListOf<String>()

    val currentPassportData = StringBuilder()
    this.forEach { line ->
        if (line.length == 0 && currentPassportData.isNotEmpty()) {
            passportData.add(currentPassportData.toString())
            currentPassportData.clear()
        } else if (line.isNotEmpty()) {
            currentPassportData.append("$line ")
        }
    }

    if (currentPassportData.isNotEmpty()) {
        passportData.add(currentPassportData.toString())
    }

    return passportData
}

fun List<String>.deserializePassportData(): List<Passport> {
    return this.map { data ->
        val passport = Passport(null, null, null, null, null, null, null, null)

        data
            .trimEnd()
            .split(' ')
            .forEach { node ->
                val keyValue = node.split(':')
                val key = keyValue[0]
                val value = keyValue[1]

                when (key) {
                    "byr" -> passport.byr = value
                    "iyr" -> passport.iyr = value
                    "eyr" -> passport.eyr = value
                    "hgt" -> passport.hgt = value
                    "hcl" -> passport.hcl = value
                    "ecl" -> passport.ecl = value
                    "pid" -> passport.pid = value
                    "cid" -> passport.cid = value
                    else -> throw UnsupportedOperationException("Passport key: '$key' is not valid.")
                }
            }

        passport
    }
}

fun Passport.hasAllRequiredFields(): Boolean {
    return this.byr != null &&
            this.ecl != null &&
            this.eyr != null &&
            this.hcl != null &&
            this.hgt != null &&
            this.iyr != null &&
            this.pid != null
}

fun Passport.isStrictValid(): Boolean {
    val hasRequiredFields = this.hasAllRequiredFields()

    if (!hasRequiredFields) {
        return false
    }

    val byrValue = this.byr!!.toIntOrNull()
    if (byrValue == null || byrValue < 1920 || byrValue > 2002) {
        return false
    }

    val iyrValue = this.iyr!!.toIntOrNull()
    if (iyrValue == null || iyrValue < 2010 || iyrValue > 2020) {
        return false
    }

    val eyrValue = this.eyr!!.toIntOrNull()
    if (eyrValue == null || eyrValue < 2020 || eyrValue > 2030) {
        return false
    }

    if (this.hgt!!.length < 3) {
        return false
    }
    val hgtSuffix = hgt!!.takeLast(2)
    val hgtValue = hgt!!.substring(0, hgt!!.length - 2).toIntOrNull() ?: return false
    when (hgtSuffix) {
        "cm" -> {
            if (hgtValue < 150 || hgtValue > 193) {
                return false
            }
        }
        "in" -> {
            if (hgtValue < 59 || hgtValue > 76) {
                return false
            }
        }
        else -> return false
    }

    if (!Regex("^#[0-9a-f]{6}\$").matches(this.hcl!!)) {
        return false
    }

    if (!Regex("^(amb|blu|brn|gry|grn|hzl|oth)\$").matches(this.ecl!!)) {
        return false
    }

    if (!Regex("^[0-9]{9}\$").matches(this.pid!!)) {
        return false
    }

    return true
}

data class Passport(
    var byr: String?,
    var iyr: String?,
    var eyr: String?,
    var hgt: String?,
    var hcl: String?,
    var ecl: String?,
    var pid: String?,
    var cid: String?
)