import utilities.ResourceManager

fun main() {
    val rawInput = ResourceManager().getResourceAsText("Day2Input.txt")

    val input = rawInput
        .split("\r\n")
        .map { serializedPasswordPolicy ->
            serializedPasswordPolicy.deserializePasswordPolicy()
        }

    val numberOfValidPasswordsPartOne = input.count { passwordPolicy ->
        val frequencyRequiredLetterAppears = passwordPolicy.password
            .count { c -> c == passwordPolicy.letterReq }

        frequencyRequiredLetterAppears >= passwordPolicy.minReq &&
                frequencyRequiredLetterAppears <= passwordPolicy.maxReq
    }

    println(numberOfValidPasswordsPartOne)

    val numberOfValidPasswordsPartTwo = input
        .count { passwordPolicy ->
            val appearsInFirstPosition = passwordPolicy.password[passwordPolicy.minReq - 1] == passwordPolicy.letterReq
            val appearsInSecondPosition = passwordPolicy.password[passwordPolicy.maxReq - 1] == passwordPolicy.letterReq

            appearsInFirstPosition xor appearsInSecondPosition
        }

    println(numberOfValidPasswordsPartTwo)
}

fun String.deserializePasswordPolicy(): PasswordPolicy {
    val splitPolicyAndPassword = this.split(':')
    val serializedPolicy = splitPolicyAndPassword[0]
    val lengthReqAndLetterReq = serializedPolicy.split(' ')
    val minReq = lengthReqAndLetterReq[0].split('-')[0].toInt()
    val maxReq = lengthReqAndLetterReq[0].split('-')[1].toInt()
    val letterReq = lengthReqAndLetterReq[1][0]

    val password = splitPolicyAndPassword[1].substring(1)

    return PasswordPolicy(minReq, maxReq, letterReq, password)
}

data class PasswordPolicy(
    val minReq: Int,
    val maxReq: Int,
    val letterReq: Char,
    val password: String
)