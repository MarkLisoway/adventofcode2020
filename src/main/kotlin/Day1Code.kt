import utilities.ResourceManager

fun main() {
    val rawInput = ResourceManager().getResourceAsText("Day1Input.txt")

    val input = rawInput.split("\r\n").map { t -> t.toInt() }

    val numberOfInputs = input.size

    twoInputsLoop@
    for (i in 0 until numberOfInputs) {
        for (j in 0 until numberOfInputs) {
            if (j == i) {
                // Skip double counting the same input
                // If we are looking for a sum of 2020, and we find 1010 in the input, we dont want to add it to itself
                continue
            }

            val sum = input[i] + input[j]
            if (sum == 2020) {
                println(input[i] * input[j])
                break@twoInputsLoop
            }
        }
    }

    threeInputsLoop@
    for (i in 0 until numberOfInputs) {
        for (j in 0 until numberOfInputs) {
            for (k in 0 until numberOfInputs) {
                if (k == i || k == j || j == i) {
                    // As before, we do not want to double (or triple) count inputs
                    continue
                }

                val sum = input[i] + input[j] + input[k]
                if (sum == 2020) {
                    println(input[i] * input[j] * input[k])
                    break@threeInputsLoop
                }
            }
        }
    }
}