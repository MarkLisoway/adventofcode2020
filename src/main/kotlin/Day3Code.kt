import utilities.ResourceManager

fun main() {
    val rawInput = ResourceManager().getResourceAsText("Day3Input.txt")

    val skiHillGrid = rawInput
        .split("\r\n")
        .map { line -> line.toCharArray() }

    val numberOfTreesHitOne = determineTreesHit(3, 1, skiHillGrid)

    println(numberOfTreesHitOne)

    val r1D1Hits: Long = determineTreesHit(1, 1, skiHillGrid)
    val r3D1Hits: Long = determineTreesHit(3, 1, skiHillGrid)
    val r5D1Hits: Long = determineTreesHit(5, 1, skiHillGrid)
    val r7D1Hits: Long = determineTreesHit(7, 1, skiHillGrid)
    val r1D2Hits: Long = determineTreesHit(1, 2, skiHillGrid)

    val productOfTreesHit: Long = r1D1Hits * r3D1Hits * r5D1Hits * r7D1Hits * r1D2Hits

    println(productOfTreesHit)
}

fun determineTreesHit(xStep: Int, yStep: Int, skiHillGrid: List<CharArray>): Long {
    val numberOfRows = skiHillGrid.size
    val numberOfColumns = skiHillGrid[0].size

    val position = Coordinate(0, 0)
    var treesHit = 0
    while (position.yCoordinate < numberOfRows) {
        if (skiHillGrid[position.yCoordinate][position.xCoordinate] == '#') {
            treesHit++
        }

        position.yCoordinate += yStep

        if (position.xCoordinate + xStep > numberOfColumns - 1) {
            // Wrap around the grid to emulate infinite terrain in the X axis
            val distanceToEnd = (numberOfColumns - 1) - position.xCoordinate
            val offset = (xStep - distanceToEnd) - 1
            position.xCoordinate = offset
        } else {
            position.xCoordinate += xStep
        }
    }

    return treesHit.toLong()
}

data class Coordinate(var xCoordinate: Int, var yCoordinate: Int)