package utilities

import java.io.File

class ResourceManager {

    fun getResourceAsText(resourceName: String): String {
        return this::class.java.classLoader.getResource(resourceName)!!.readText()
    }

}