# Advent of Code 2020

My solutions to Pandell's [Advent of Code 2020](https://adventofcode.com/2020).
Decided to do this in [Kotlin](https://kotlinlang.org/) to mix it up a bit (but still stay close to my familiarity with C#).